# 这套Android面试题汇总大全，希望对大家有帮助哈~

# 全部答案，更新日期：2月7日，直接下载吧

# 全部答案，更新日期：2023年2月7日，直接下载吧！

# 下载链接：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

## 1、请简述一下什么是 Kotlin？它有哪些特性？
kotlin和java一样也是一门jvm语言最后的编译结果都是.class文件,并且可以通过kotlin的.class文件反编译回去java代码,并且封装了许多语法糖,其中我在项目中常用的特性有

1.扩展,(使用非集成的方式 扩张一个类的方法和变量):比方说 px和dp之间的转换 之前可能需要写个Util现在,通过扩展Float的变量 最后调用的时候仅仅是 123.dp这样px转成dp了

2.lamdba表达式,函数式编程. lamdba表达式并不是kotlin 的专利,java中也有,但是有限制, 像setOnClickListener一样, 接口方法只有一个的情况才能调用, 而在kotlin中对接口的lambda也是如此,有这样的限制,但是他更推荐你使用闭包的方式而不是实现匿名接口的方式去实现这样的功能,闭包对lambda没有接口这么多的限制,另外就是函数式编程 在java8中提供了streamApi对集合进行mapsortreduce等等操作,但是对androidapi有限制,为了兼容低版本,几乎不可能使用streamApi

3.判空语法 省略了许多ifxxx==null的写法 也避免了空指针异常aaa?.toString ?: "空空如也" 当aaa为空的时候 它的值被"空空如也"替代
aaa?.let{ it. bbb
}
当aaa不为空时 执行括号内的方法

4.省略了findViewById ,使用kotlin 就可以直接用xml中定义的id 作为变量获取到这个控件,有了这个 butterknife就可以淘汰了,使用databinding也能做到,但是,非常遗憾,databinding的支持非常不好,每次修改视图,都不能及时生成,经常要rebulid才能生成.

5,默认参数 减少方法重载 fun funName(a :Int ,b:Int =123)通过如上写法 实际在java中要定义两个写法 funName(a)和funName(a,b)

6.kotlin无疑是android将来语言的趋势,我已经使用kotlin 一年了,不仅App工程中使用,而且封装的组件库也是用kotlin,另外说明,kotlin会是apk大小在混淆后增加几百k.但对于更舒适的代码来说这又算的了什么呢

## 2、Kotlin 中注解 @JvmOverloads的作用？
@JvmOverloads注解的作用就是:在有默认参数值的方法加上
@JvmOverloads注解，则Kotlin就会暴露多个重载方法。可以减少写构造方法。例如：没有加注解，默认参数没有起到任何作用。

```
fun f(a: String, b: Int = 0, c: String="abc")
{
...
}
那相当于在java中：void f(String a, int b, String c){
}
如果加上注解@JvmOverloads ，默认参数起到作用
@JvmOverloads
fun f(a: String, b: Int = 0, c: String="abc")
{
...}
相当于Java中：
三个构造方法，
void f(String a)
void f(String a, int b)
void f(String a, int b, String c)
```

## 3、Kotlin中List与MutableList的区别？

List返回的是EmptyList，MutableList返回的是一个ArrayList，查看EmptyList的源码就知道了，根本就没有提 供add 方法。

```
internal object EmptyList : List, Serializable, RandomAccess {
private const val serialVersionUID: Long =
-7390468764508069838L
override fun equals(other: Any?): Boolean = other is
List<*> && other.isEmpty() override fun hashCode():
Int = 1
override fun toString(): String = "[]" override val size: Int get() = 0 override fun
isEmpty(): Boolean = true override funcontains(element: Nothing):
Boolean = false
override fun containsAll(elements:
Collection<Nothing>): Boolean =
elements.isEmpty()
override fun get(index: Int): Nothing = throw
IndexOutOfBoundsException("Empty list doesn't contain
element at index $index.")
override fun indexOf(element: Nothing): Int =
-1
override fun lastIndexOf(element: Nothing):
Int = -1
override fun iterator(): Iterator<Nothing> =
EmptyIterator
override fun listIterator(): ListIterator<Nothing>
= EmptyIterator override fun listIterator(index:
Int): ListIterator<Nothing> {
if (index != 0) throw
IndexOutOfBoundsException("Index: $index")
return EmptyIterator
}override fun subList(fromIndex: Int, toIndex: Int):
List<Nothing> {
if (fromIndex == 0 && toIndex == 0) return
this
throw
IndexOutOfBoundsException("fromIndex:
$fromIndex, toIndex: $toIndex")
}
private fun readResolve(): Any = EmptyList
```

## 3、Kotlin中实现单例的几种常见方式？

- 饿汉式：

  ```
  object StateManagementHelper {
  fun init() {
  //do some initialization works
  }
  }
  ```

- 懒汉式:

```
class StateManagementHelper private
constructor(){
companion object { private
var instance:
StateManagementHelper? = null
@Synchronized get()
{ if (field == null)
field =
StateManagementHelper()
return field
}
}
fun init() {
//do some initialization works
}}
```

- 双重检测：

```
class StateManagementHelper private
constructor(){
companion object {
val instance: StateManagementHelper
by lazy(mode =
LazyThreadSafetyMode.SYNCHRONIZED)
{ StateManagementHelper() }
}
fun init() {
//do some initialization works
}
}
```

- 静态内部类：

```
class StateManagementHelper private
constructor(){
companion object
{ val INSTANCE =
StateHelperHolder.holder
}
private object StateHelperHolder {
val holder = StateManagementHelper()}
fun init() {
//do some initialization works
}
}
```

## 4、什么是委托属性？请简要说说其使用场景和原理？

属性委托
有些常见的属性操作,我们可以通过委托方式,让它实现,例如:

- azy 延迟属性: 值只在第一次访问的时候计算
- observable 可观察属性:属性发生改变时通知
- map 集合: 将属性存在一个map集合里面



类委托
可以通过类委托来减少 extend类委托的时,编译器回优使用自身重新函数,而不是委托对象的函数

```
interface
Base{ fun
print()
}
case BaseImpl(var x: Int):Base{
override fun
print(){ print(x)
}
}
// Derived 的 print 实现会通过构造函数的b对象来完成
class Derived(b: base): Base by b
```

## 5、请举例说明Kotlin中with与apply函数的应用场景和区别？
with` 不怎么使用，因为它确实不防空；
经常使用的是 `run` 和 `apply

1. run 闭包返回结果是闭包的执行结果； apply 返回的是调用者本身。
2. 使用上的差别： run 更倾向于做一些其他复杂逻辑操作，而 apply 更多的是对调用者自身配置。
3. 大部分情况下，如果不是对调用者本身进行设置，我会使用 run。

## 6、Kotlin中 Unit 类型的作用以及与Java中Void 的区别？

Unit : Kotlin 中Any的子类, 方法的返回类型为Unit时，可以省略；
Void：Java中的方法无法回类型时使用，但是不能省略；
Nothing：任何类型的子类，编译器对其有优化，有一定的推导能力，另外其常常和抛出异常一起使用；

## 7、Kotlin 中infix 关键字的原理和使用场景？

## 8、Kotlin中的可见性修饰符有哪些？相比于Java有什么区别？

## 9、你觉得Kotlin与Java混合开发时需要注意哪些问题？

## 10、在Kotlin中，何为解构？该如何使用？

## 11、在Kotlin中，什么是内联函数？有什么作用？

## 12、谈谈kotlin中的构造方法？有哪些注意事项？

## 14、谈谈Kotlin中的Sequence，为什么它处理集合操作更加高效？

## 15、请谈谈Kotlin中的Coroutines，它与线程有什么区别？有哪些优点？

# 由于篇幅原因，此处仅展示部分内容，查看更多Android面试题点击直接查看

# 全部答案，更新日期：2023年2月7日，直接下载吧！

# 下载链接：[全部答案，整理好了](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

# 新增：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

