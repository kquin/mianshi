# 这套Android面试题汇总大全，希望对大家有帮助哈~

# 全部答案，更新日期：2月7日，直接下载吧

# 全部答案，更新日期：2023年2月7日，直接下载吧！

# 下载链接：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

## 1、请回答一下Android进程间的通信方式？

![在这里插入图片描述](https://img-blog.csdnimg.cn/dd9e73b3c2e54874b56cfc88fbc6102f.png)

## 2、请谈谈你对Binder机制的理解？

Binder机制：

1.为了保证进程空间不被其他进程破坏或干扰，Linux中的 进程是相互独立或相互隔离的。
2.进程空间分为用户空间和内核空间。用户空间不可以进行数据交互；内核空间可以进行数据交互，所有进程共用一个内核空间。
3.Binder机制相对于Linux内传统的进程间通信方式：
（1）性能更好；Binder机制只需要拷贝数据一次，管道、 消息队列、Socket等都需要拷贝数据两次；而共享内存虽然不需要拷贝，但实现复杂度高。

（2）安全性更高；Binder机制通过UID/PID在内核空间添加了身份标识，安全性更高。

4.Binder跨进程通信机制：基于C/S架构，由Client、Server、ServerManager和Binder驱动组成。
5.Binder驱动实现的原理：通过内存映射，即系统调用了mmap（）函数。
6.Server Manager的作用：管理Service的注册和查询。
7.Binder驱动的作用：

（1）传递进程间的数据，通过系统 调用mmap（）函数；

（2）实现线程的控制，通过Binder 驱动的线程池，并由Binder驱动自身进行管理。
8.Server进程会创建很多线程处理Binder请求，这些线程采用Binder驱动的线程池，由Binder驱动自身进行管理。一个进程的Binder线程池默认最大是16个，超过的请求会 阻塞等待空闲的线程。
9.Android中进行进程间通信主要通过Binder类（已经实现了IBinder接口），即具备了跨进程通信的能力。

## 3、谈谈 AIDL？

AIDL是一种辅助工具,不用AIDL ,一样可以实现跨进程通讯
AIDL 的原理是binder,真正有跨进程通讯能力的也是Binder,所 以 AIDL 只是一个能帮你少写代码,少出错的辅助工具,由于设计的太好,使用太方便,所以非常常用就像 retrofit和okhttp关系一样,retrofit提供 更加友好的api,真正的网络请求还是由 okhttp发起的

# 由于篇幅原因，此处仅展示部分内容，查看更多Android面试题点击直接查看

# 全部答案，更新日期：2023年2月6日，直接下载吧！

# 下载链接：[全部答案，整理好了](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

# 新增：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )



