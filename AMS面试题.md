# 这套Android面试题汇总大全，希望对大家有帮助哈~

# 全部答案，更新日期：2月8日，直接下载吧

# 全部答案，更新日期：2023年2月8日，直接下载吧！

# 下载链接：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

## 1、ActivityManagerService是什么？什么时候初始化的？有什么作用？

ActivityManagerService 主 要负责系统中四大组件的启动、切换、调度及应用进程的管理和调度等工作 ，其 职责与操作系统中的进程管理和调度模块类似。ActivityManagerService进行初始化的时机很明确 ，就是在 SystemServer进 程开启的时候 ，就会初始化ActivityManagerService。 (系 统 启 动 流 程 )如 果打开一个App的话 ，需要AMS去通知zygote进程 ，所 有 的 Activity的 生 命 周 期 AMS来 控 制

## 2、ActivityThread是什么？ApplicationThread是什么？他们的区别

ActivityThread
在 Android中它 就代 表了 Android的主 线程 ,它是 创建 完新 进程 之后 ,main函数 被加 载 ，然后 执行 一个loop的循 环使当前线程进入消息循环 ，并且作为主线程。
ApplicationThread
ApplicationThread是 ActivityThread的 内 部 类 ，是 一 个 Binder对 象 。 在 此 处 它 是 作 为 IApplicationThread对 象的server端等待client端的请求然后进行处理 ，最大的client就是AMS。

## 3、 Instrumentation是什么？和ActivityThread是什么关系？

AMS与 ActivityThread之 间 诸 如 Activity的 创 建 、暂 停 等 的 交 互 工 作 实 际 上 是 由 Instrumentation具 体操 作 的 。 每个 Activity都 持 有 一 个 Instrumentation对 象 的 一 个 引 用 ，整 个 进 程 中 是 只 有 一 个 Instrumentation。
mInstrumentation的 初 始 化 在 ActivityThread: : handleBindApplication函 数 。

可以用来独立地控制某个组件的生命周期。Activity`的`startActivity`方法。 `startActivity`会调用`mInstrumentation .execStartActivity() ; mInstrumentation 掉用 AMS , AMS 通过 socket 通信 告知 Zygote 进程 fork 子进 程。

## 4、ActivityManagerService和zygote进程通信是如何实现的？

应 用启动时,Launcher进程请求AMS。 AMS发送创建应用进程请求 ，Zygote进程接受请求并fork应用进程 客户端发送请求调 用 Process.start() 方法新建进程连 接 调 用 的 是 ZygoteState.connect() 方 法 ， ZygoteState 是 ZygoteProcess 的 内 部 类 。ZygoteState里 用 的 LocalSocket 

```
public static ZygoteState connect( LocalSocketAddress address) throws IOException {
DataInputStream zygoteInputStream = null ;
BufferedWriter zygoteWriter = null;
final LocalSocket zygoteSocket = new LocalSocket( ) ;
try { zygoteSocket . connect(address); zygoteInputStream = new DataInputStream( zygoteSocket .getInputStream()) ; zygoteWriter = new BufferedWriter( new OutputStreamWriter( zygoteSocket .getOutputStream()) , 256) ;
} catch (IOException ex) {
try { zygoteSocket .close() ;
} catch (IOException ignore) {
}throw ex ;
}return new ZygoteState(zygoteSocket , zygoteInputStream , zygoteWriter , Arrays .asList(abiListString .split(","))) ;
}
```

Zygote 处理客户端请求
Zygote 服 务 端 接 收 到 参 数 之 后 调 用 ZygoteConnection.processOneCommand() 处 理 参 数 ，并fork 进 程 最 后 通过 fifindStaticMain() 找到 ActivityThread 类的 main() 方法并执行 ，子进程就启动了

## 5、ActivityManager、ActivityManagerService、ActivityManagerNative、ActivityManagerproxy的关系

## 6、手写实现简化版AMS

# 由于篇幅原因，此处仅展示部分内容，查看更多Android面试题点击直接查看

# 全部答案，更新日期：2023年2月8日，直接下载吧！

# 下载链接：[全部答案，整理好了](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

# 新增：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )



