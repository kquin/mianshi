# Android企业常问面试题

#### 介绍
该仓库分享了2158个朋友自己的Android面试真经，并提供了参考答案。面试并不是唯一目的，重要的是可以通过面试题更加系统高效地提升自己的技能

包括 Java基础、Java集合、Java多线程、Java虚拟机、Android 四大组件、Android 异步任务和消息机制、 Android UI 绘制、 Android 性能调优、 Android 中的 IPC、Android 系统 SDK 相关、第三方框架、数据结构、设计模式、计算机网络、Kotlin、音视频开发、 Flutter、算法、系统启动流程、Binder、Handler、AMS、字节跳动，阿里巴巴、腾讯，华为，小米、等大厂面试题等、等技术栈！应届生，实习生，企业工作过的，都可参考学习!

## 1600+ 道，面试题技术大类，索引，直达

| [Java基础](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Java%E5%9F%BA%E7%A1%80%E9%9D%A2%E8%AF%95%E9%A2%98.md) | [Java集合](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Java%E9%9B%86%E5%90%88%E9%9D%A2%E8%AF%95%E9%A2%98.md) | [Java多线程](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Java%E5%A4%9A%E7%BA%BF%E7%A8%8B%E9%9D%A2%E8%AF%95%E9%A2%98.md) | [ **Java虚拟机** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Java%E8%99%9A%E6%8B%9F%E6%9C%BA.md) | [Android 四大组件](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Android%E5%9B%9B%E5%A4%A7%E7%BB%84%E4%BB%B6%E9%9D%A2%E8%AF%95%E9%A2%98.md) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [ **Android 异步任务和消息机制** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Android%E5%BC%82%E6%AD%A5%E4%BB%BB%E5%8A%A1%E5%92%8C%E6%B6%88%E6%81%AF%E6%9C%BA%E5%88%B6%E9%9D%A2%E8%AF%95%E9%A2%98.md) | [ **Android UI绘制相关** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Android%20UI%E7%BB%98%E5%88%B6%E7%9B%B8%E5%85%B3%E9%9D%A2%E8%AF%95%E9%A2%98.md#%E8%BF%99%E5%A5%97android%E9%9D%A2%E8%AF%95%E9%A2%98%E6%B1%87%E6%80%BB%E5%A4%A7%E5%85%A8%E5%B8%8C%E6%9C%9B%E5%AF%B9%E5%A4%A7%E5%AE%B6%E6%9C%89%E5%B8%AE%E5%8A%A9%E5%93%88) | [ **Android 性能调优** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Android性能优化相关面试题.md) | [ **Android 中的 IPC** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Android%20%E4%B8%AD%E7%9A%84IPC%E9%9D%A2%E8%AF%95%E9%A2%98.md#3%E8%B0%88%E8%B0%88-aidl) | [**Android 系统 SDK 相关**](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Android%20%E7%B3%BB%E7%BB%9FSDK%E7%9B%B8%E5%85%B3%E9%9D%A2%E8%AF%95%E9%A2%98.md#%E8%BF%99%E5%A5%97android%E9%9D%A2%E8%AF%95%E9%A2%98%E6%B1%87%E6%80%BB%E5%A4%A7%E5%85%A8%E5%B8%8C%E6%9C%9B%E5%AF%B9%E5%A4%A7%E5%AE%B6%E6%9C%89%E5%B8%AE%E5%8A%A9%E5%93%88) |
| [ **第三方框架** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/第三方框架分析面试题.md) | [ **数据结构** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/数据结构方面面试题.md) | [ **设计模式** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/设计模式面试题.md) | [ **计算机网络** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/计算机网络方面面试题.md) | [ **Kotlin** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Kotlin面试题.md) |
| [ **音视频开发** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/音视频开发高频面试题.md) | [ **Flutter** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Flutter高频面试题.md) | [ **算法** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/算法高频面试题.md) | [ **系统启动流程** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/系统启动流程面试题.md) | [ **Binder** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Binder面试题.md) |
| [ **Handler** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/Handler面试题.md) | [ **AMS** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/AMS面试题.md) | [ **企业常见面试题** ](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/企业常见174道面试题.md) |                                                              |                                                              |

## 全部答案，更新日期：2023年2月3日


## 下载地址：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

## 下载地址：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Java基础面试题

1、抽象类与接口的区别？

2、Java 中深拷贝与浅拷贝的区别？

3、谈谈Error和Exception的区别？

4、什么是反射机制？反射机制的应用场景有哪些？

5、谈谈你对Java泛型中类型擦除的理解，并说说其局限性？

6、说说你对Java注解的理解？

7、谈一谈Java成员变量，局部变量和静态变量的创建和回收时机？

8、请说说Java中String.length()的运作原理？

9、扩展：内部类都有哪些？

10、多态概述

11、多态中成员的特点

12、instanceof关键字

13、多态的转型

14、java中接口和继承的区别

15、线程池的好处

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Java集合面试题

1.谈谈List,Set,Map的区别？

2.谈谈ArrayList和LinkedList的区别？

3.请说一下HashMap与HashTable的区别

4.谈一谈ArrayList的扩容机制？

5.HashMap 的实现原理？

6.请简述 LinkedHashMap 的工作原理和使用方式？

7.谈谈对于ConcurrentHashMap的理解?

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Java多线程面试题

1、Java 中使用多线程的方式有哪些？

2、说一下线程的几种状态？

3、如何实现多线程中的同步？

4、谈谈线程死锁，如何有效的避免线程死锁？

5、如何避免死锁

6、请谈谈 Thread 中 run() 与 start()的区别？

7、synchronized和volatile关键字的区别？

8、如何保证线程安全？

9、谈谈ThreadLocal用法和原理？

10、Java 线程中notify 和 notifyAll有什么区别？

11、什么是线程池？如何创建一个线程池？

12、谈一谈java线程常见的几种锁？

13、谈一谈线程sleep()和wait()的区别？

14、什么是悲观锁和乐观锁？

15、什么是BlockingQueue？请分析一下其内部原理并谈谈它的使用场景？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Java虚拟机面试题

1、谈一谈JAVA垃圾回收机制？

2、怎么找到无用对象？

3、如何释放无用对象？

4、何时开始GC？

5、回答一下什么是强、软、弱、虚引用以及它们之间的区别？

6、简述JVM中类的加载机制与加载过程？

7、JVM、Dalvik、ART三者的原理和区别？

8、请谈谈Java的内存回收机制？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Android 四大组件面试题

1、Activity 与 Fragment 之间常见的几种通信方式？

2、LaunchMode 的应用场景？

3、BroadcastReceiver 与LocalBroadcastReceiver 有什么区别？

4、对于 Context，你了解多少?

5、IntentFilter是什么？有哪些使用场景？

6、谈一谈startService和bindService的区别，生命周期以及使用场景？

7、 Service如何进行保活？

8、简单介绍下ContentProvider是如何实现数据共享的？

9、说下切换横竖屏时Activity的生命周期?

10、Activity中onNewIntent方法的调用时机和使用场景？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Android 异步任务和消息机制面试题

1、HandlerThread 的使用场景和用法？

2、IntentService 的应用场景和使用姿势？

3、AsyncTask的优点和缺点？

4、AsyncTask只能执行一次execute()方法，那么为什么用线程池队列管理 ？

5、AsyncTask的onPreExecute()、doInBackground()、onPostExecute()方法的调用流程？

6、谈谈你对 Activity.runOnUiThread 的理解？

7、子线程能否更新UI？为什么？

8、谈谈 Handler 机制和原理？

9、为什么在子线程中创建Handler会抛异常？

10、试从源码角度分析Handler的post和sendMessage方法的区别和应用场景？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Android UI 绘制面试题

1、Android 补间动画和属性动画的区别？

2、简述一下 Android 中 UI 的刷新机制？

3、LinearLayout, FrameLayout,RelativeLayout 哪个效率高, 为什么？

4、Window和DecorView是什么?DecorView又是如何和Window建立联系的?

5、谈谈Android的事件分发机制？

6、谈谈自定义View的流程？

7、针对RecyclerView你做了哪些优化？

8、谈谈如何优化ListView？

9、谈谈自定义LayoutManager的流程？

10、什么是 RemoteViews？使用场景有哪些？

11、谈一谈获取View宽高的几种方法？

12、谈一谈插值器和估值器？

13、getDimension、getDimensionPixelOffset 和getDimensionPixelSize 三者的区别？

14、请谈谈源码中StaticLayout的用法和应用场景？

15、有用过ConstraintLayout吗？它有哪些特点？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Android 性能调优面试题

1、谈谈你对Android性能优化方面的了解

2、一般什么情况下会导致内存泄漏问题？

3、自定义 Handler 时如何有效地避免内存泄漏问题？

4、哪些情况下会导致oom问题？

5、ANR 出现的场景以及解决方案？

6、谈谈Android中内存优化的方式？

7、谈谈布局优化的技巧？

8、Android 中的图片优化方案？

9、AndroidNativeCrash问题如何分析定位？

10、谈谈怎么给apk瘦身？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Android 中的 IPC面试题

1、请回答一下Android进程间的通信方式？

2、请谈谈你对Binder机制的理解？

3、谈谈 AIDL？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Android 系统 SDK 相关面试题

1、请简要谈谈Android系统的架构组成？

2、SharedPreferences 是线程安全的吗？它的 commit 和 apply 方法有什么区别？

3、Serializable和Parcelable的区别?

4、请简述一下 Android 7.0 的新特性？

5、谈谈ArrayMap和HashMap的区别？

6、简要说说 LruCache 的原理？

7、为什么推荐用SparseArray代替HashMap？

8、PathClassLoader和DexClassLoader有何区别？

9、说说HttpClient与HttpUrlConnection的区别？并谈谈为何前者会被替代？

10、什么是Lifecycle？请分析其内部原理和使用场景？

11、谈一谈Android的签名机制？

12、谈谈安卓apk构建的流程？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)


### 第三方框架面试题

1、谈一谈LeakCanray的工作原理？

2、谈一谈EventBus的原理？

3、谈谈网络请求中的拦截器（Interceptor）？

4、谈一谈Glide的缓存机制？

5、ViewModel的出现是为了解决什么问题？并简要说说它的内部原理？

6、请说说依赖注入框架ButterKnife的实现原理？

7、谈一谈RxJava背压原理？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)


### 数据结构面试题

1、什么是冒泡排序？如何优化？

2、请用 Java 实现一个简单的单链表？

3、如何反转一个单链表？

4、谈谈你对时间复杂度和空间复杂度的理解？

5、谈一谈如何判断一个链表成环？

6、什么是红黑树？为什么要用红黑树？

7、什么是快速排序？如何优化？

8、说说循环队列？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### 设计模式面试题

1、请简要谈一谈单例模式？

2、对于面向对象的六大基本原则了解多少？

3、请列出几种常见的工厂模式并说明它们的用法？

4、说说项目中用到的设计模式和使用场景？

5、什么是代理模式？如何使用？Android源码中的代理模式？

6、谈一谈单例模式，建造者模式，工厂模式的使用场景？如何合理选择

7、谈谈你对原型模式的理解？

8、请谈谈策略模式原理及其应用场景？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### 计算机网络面试题

1、请简述 Http 与 Https 的区别？

2、说一说https,udp,socket区别？

3、请简述一次http网络请求的过程？

4、谈一谈TCP/IP三次握手，四次挥手？

5、为什么说Http是可靠的数据传输协议？

6、TCP/IP协议分为哪几层？TCP和HTTP分别属于哪一层？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Kotlin面试题

1、请简述一下什么是 Kotlin？它有哪些特性？

2、Kotlin 中注解 @JvmOverloads的作用？

3、Kotlin中List与MutableList的区别？

4、Kotlin中实现单例的几种常见方式？

5、什么是委托属性？请简要说说其使用场景和原理？

6、Kotlin中 Unit 类型的作用以及与Java中Void 的区别？

7、Kotlin 中infix 关键字的原理和使用场景？

8、Kotlin中的可见性修饰符有哪些？相比于Java有什么区别？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### 音视频开发面试题

1、为什么巨大的原始视频可以编码成很小的视频呢?这其中的技术是什么呢?

2、怎么做到直播秒开优化？

3、直方图在图像处理里面最重要的作用是什么？

4、数字图像滤波有哪些方法？

5、图像可以提取的特征有哪些？

6、衡量图像重建好坏的标准有哪些？怎样计算？

7、AAC和PCM的区别？

8、H264存储的两个形态？

9、FFMPEG:图片如何合成视频

10、常见的音视频格式有哪些？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)


### Flutter面试题

1、Dart 语言的特性？

2、Dart的一些重要概念？

3、Dart 当中的 「..」表示什么意思？

4、Dart 多任务如何并行的？

5、dart是值传递还是引用传递？

6、Flutter 是什么？

7、Flutter 特性有哪些？

8、Flutter 和 Dart的关系是什么？

9、Widget 和 element 和 RenderObject 之间的关系？

10、mixin extends implement 之间的关系?

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)


### 算法面试题

1、如何⾼效寻找素数

2、如何运⽤⼆分查找算法

3、如何⾼效解决接⾬⽔问题

4、如何⾼效进⾏模幂运算

5、如何去除有序数组的重复元素

6、如何寻找最⻓回⽂⼦串

7、如何运用贪心思想广域玩跳跃游戏

8、如何K个一组反转链表

9、如何判定括号合法性

10、如何寻找消失的元素

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### 系统启动流程面试题

1、你了解Android系统启动流程吗？

2、system_server为什么要在Zygote中启动，而不是由init直接启动

3、为什么要专门使用Zygote进程去孵化应用进程，而不是让system_server去孵化呢？

4、能说说具体是怎么导致死锁的吗？

5、Zygote为什么不采用Binder机制进行IPC通信？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Binder面试题

1、Binder有什么优势

2、Binder是如何做到一次性拷贝的

3、MMAP的内存映射原理了解吗？

4、Binder机制是如何跨进程的

5、说说四大组件通信

6、为什么Intent不能传递大数据

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### Handler面试题

1、HandlerThread是什么？为什么它会存在？

2、简述下Handler机制的总体原理

3、Looper存在哪？如何可以保证线程独有？

4、如何理解ThreadLocal的作用？

5、主线程Main Looper和一般的Looper的异同？

6、Handler或者说Looper如何切换线程？

7、Looper的loop（）死循环为什么不卡死？

8、Looper的等待是如何能够准确唤醒的？

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### AMS面试题

1、ActivityManagerService是什么？什么时候初始化的？有什么作用？

2、ActivityThread是什么？ApplicationThread是什么？他们的区别

3、 Instrumentation是什么？和ActivityThread是什么关系？

4、ActivityManagerService和zygote进程通信是如何实现的？

5、ActivityManager、ActivityManagerService、ActivityManagerNative、ActivityManagerproxy的关系

6、手写实现简化版AMS

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

### 企业常见面试题

1、SD卡

2、.android的数据存储方式

3、BroadcastReceiver

4、sp频繁操作会有什么后果?sp能存多少数据?

5、dvm与jvm的区别

6、ART

7、Activity的生命周期

8、Application能不能启动Activity

9、Activity的状态都有哪些

10、横竖屏切换时Activity的生命周期

11、如何设置activity成窗口样式

12、Activity的启动方式

13、Service的生命周期

14、IntentService

15、Fragment和Activity的onCreateOptionsMenu

16、Service的onStartCommand有几种返回值

17、Service的onRebind什么情况下执行

18、Handler防止内存泄露

19、IntentFilter的匹配法则

20、Fragment与Activity传值

[此处只展现部分内容，查看更多内容......](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)