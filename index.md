![输入图片说明](../Plug-in%20picture/%E9%BB%98%E8%AE%A4%E6%A0%87%E9%A2%98__2023-02-06+21_45_39.jpeg)

![在这里插入图片描述](https://img-blog.csdnimg.cn/0736ba8ec5744a6b9d2f141651bcb372.png)

同时还整理了一套移动架构师笔记，特别适合有3-5年以上经验的小伙伴深入学习提升：

主要包括阿里，以及字节跳动，腾讯，华为，小米，等一线互联网公司主流架构技术。如果你有需要，尽管拿走好了。

![在这里插入图片描述](https://img-blog.csdnimg.cn/ce5c7e1347e94850b77236b820f28c25.png)
![输入图片说明](../Plug-in%20picture/5%5D2A8EZLW%25NOE_ON3@J9%7D73.png)
## 第一章 Java方面

- 第一节 Java基础
- 第二节 Java集合
- 第三节 Java多线程
- 第四节 Java虚拟机

![在这里插入图片描述](https://img-blog.csdnimg.cn/57014b7745e74fceb3ea0214c1f9b218.png)

## 第二章 Android 方面

- 第一节 Android 四大组件相关
- 第二节 Android 异步任务和消息机制
- 第三节 Android UI 绘制相关
- 第四节 Android 性能调优相关
- 第五节 Android 中的 IPC
- 第六节 Android 系统 SDK 相关
- 第七节 第三方框架分析
- 第八节 综合技术
- 第九节 数据结构方面
- 第十节 设计模式
- 第十一节 计算机网络方面
- 第十二节 Kotlin方面

![在这里插入图片描述](https://img-blog.csdnimg.cn/e138e275d982466f8dbf882694788338.png)

## 第三章 音视频开发高频面试题

- 为什么巨大的原始视频可以编码成很小的视频呢?这其中的技术是什么呢?
- 怎么做到直播秒开优化？
- 直方图在图像处理里面最重要的作用是什么？
- 数字图像滤波有哪些方法？
- 图像可以提取的特征有哪些？
- .......

![在这里插入图片描述](https://img-blog.csdnimg.cn/d64479fbad9e4dfaa051b07517f329e4.png)




## 第四章 Flutter高频面试题

- 第一节 Dart部分
  * Dart 语言的特性？
  * Dart的一些重要概念？
  * dart是值传递还是引用传递？
  * Dart 多任务如何并行的？
  * 说一下 mixin？
  * ......

- 第二节 Flutter 部分
  * Flutter 特性有哪些？
  * Flutter 中的生命周期
  * Widget 和 element 和 RenderObject 之间的关系？
  * mixin extends implement 之间的关系?
  * Flutter 和 Dart的关系是什么？
  * ......

![在这里插入图片描述](https://img-blog.csdnimg.cn/d62410b21393429ca8624fcba908fba9.png)


## 第五章 算法高频面试题

- 如何⾼效寻找素数
- 如何运⽤⼆分查找算法
- 如何⾼效解决接⾬⽔问题
- 如何去除有序数组的重复元素
- 如何⾼效进⾏模幂运算
- ......

![在这里插入图片描述](https://img-blog.csdnimg.cn/52616a89875d49b4a3af2047a598ae54.png)

## 第六章 Android Framework方面

- 第一节 系统启动流程面试题解析
- 第二节 Binder面试题解析
- 第三节 Handler面试题解析
- 第四节 AMS面试题解析
- ......

![在这里插入图片描述](https://img-blog.csdnimg.cn/fddbcdd3088e4e4bb6fdd462065f4de1.png)

## 第七章 企业常见174道面试题

- 1.SD卡
- 2.android的数据存储方式
- 3.BroadcastReceiver
- 4.sp频繁操作会有什么后果?sp能存多少数据?
- 5.dvm与jvm的区别
- 6.ART
- 7.Activity的生命周期
- 8.Application能不能启动Activity
- 9.Activity的状态都有哪些
- 10.横竖屏切换时Activity的生命周期 
- .......

![在这里插入图片描述](https://img-blog.csdnimg.cn/74721170f94348f1a3bee7eced0509a1.png)