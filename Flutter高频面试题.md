# 这套Android面试题汇总大全，希望对大家有帮助哈~

# 全部答案，更新日期：2月7日，直接下载吧

# 全部答案，更新日期：2023年2月7日，直接下载吧！

# 下载链接：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

## 1、Dart 语言的特性？

Productive（生产力高，Dart的语法清晰明了，工具简单但功能强大）
Fast（执行速度快，Dart提供提前优化编译，以在移动设备和Web上获得可预测的高性能和快速启动。）
Portable（易于移植，Dart可编译成ARM和X86代码，这样Dart移动应用程序可以在iOS、Android和其他地方运行）
Approachable（容易上手，充分吸收了高级语言特性，如果你已经知道C++，C语言，或者Java，你可以在短短几天内用Dart来开发）
Reactive（响应式编程）

## 2、Dart的一些重要概念？

在Dart中，一切都是对象，所有的对象都是继承自Object
Dart是强类型语言，但可以用var或 dynamic来声明一个变量，Dart会自动推断其数据类型,dynamic类似c#
没有赋初值的变量都会有默认值null
Dart支持顶层方法，如main方法，可以在方法内部创建方法
Dart支持顶层变量，也支持类变量或对象变量
Dart没有public protected private等关键字，如果某个变量以下划线（_）开头，代表这个变量在库中是私有的

## 3、Dart 当中的 「..」表示什么意思？

Dart 当中的 「..」意思是 「级联操作符」，为了方便配置而使用。
「..」和「.」不同的是 调用「..」后返回的相当于是 this，而「.」返回的则是该方法返回的值 。
2. Dart 的作用域
Dart 没有 「public」「private」等关键字，默认就是公开的，私有变量使用 下划线 _开头。
3. Dart 是不是单线程模型？是如何运行的？
Dart 是单线程模型，如何运行的看这张图：引用《Flutter中文网》里的话：
Dart 在单线程中是以消息循环机制来运行的，其中包含两个任务队列，一个是“微任务队
列” microtask queue，另一个叫做“事件队列” event queue。
入口函数 main() 执行完后，消息循环机制便启动了。首先会按照先进先出的顺序逐个执
行微任务队列中的任务，当所有微任务队列执行完后便开始执行事件队列中的任务，事件
任务执行完毕后再去执行微任务，如此循环往复，生生不息。

## 4、Dart 多任务如何并行的？

在dart中，一个Isolate对象其实就是一个isolate执行环境的引用，一般来说我们都是通过当前的isolate去控制其他的isolate完成彼此之间的交互，而当我们想要创建一个新的Isolate可以使用Isolate.spawn方法获取返回的一个新的isolate对象，两个isolate之间使用SendPort相互发送消息，而isolate中也存在了一个与之对应的ReceivePort接受消息用来处理，但是我们需要注意的是，ReceivePort和SendPort在每个isolate都有一对，只有同一个isolate中的ReceivePort才能接受到当前类的SendPort发送的消息并且处理。

## 5、dart是值传递还是引用传递？

dart是值传递。我们每次调用函数，传递过去的都是对象的内存地址，而不是这个对象的复制。先来看段代码

```
main(){
Test a = new Test(5);
print("a的初始值为：${a.value}");
setValue(a);
print("修改后a的值为: ${a.value}");
}
class Test{
int value = 1;Test(int newValue){
this.value = newValue;
}
}
setValue(Test s){
print("修改value为100");
s.value = 100;
}
```

输出结果为

```
a的初始值为：5
修改value为100
修改后a的值为:100
```

从这里可以看出是值传递，如果只是复制了一个对象，然后把这个新建的对象地址传递到函数里面的话，setValue()函数中的修改是不会影响到main函数中的a的，因为二者所引用的内存地址是不一样。有些人可能会以以下代码反驳我：

```
main(){
int s = 6;
setValue(s);
print(s); //输出6，而不是7
}
class Test{
int value = 1;
Test(int newValue){
this.value = newValue;}
}
setValue(int s){
s += 1;
}
```



你看，这输出的不是6吗，在dart中一切皆为对象，如果是引用传递，那为什么是6啊。
答案是这样的，在setValue()方法中，参数s实际上和我们初始化int s = 6的s不是一个对象，只是他们现在指的是同一块内存区域，然后在setValue()中调用s += 1的时候，这块内存区域的对象执行+1操作，然后在堆(类比java)中产生了一个新的对象，s再指向这个对象。所以s参数只是把main函数中的s的内存地址复制过去了，就比如java中的：

```
public class Test {
public static void main(String[] args) {
Test a = new Test();
Test b = a;
b = new Test();
}
}
```

我们只要记住一点，参数是把内存地址传过去了，如果对这个内存地址上的对象修改，那么其他位置的引用该内存地址的变量值也会修改。千万要记住dart中一切都是对象。



## 6、Flutter 是什么？

Flutter是谷歌的移动UI框架，可以快速在iOS和Android上构建高质量的原生用户界面。 Flutter可以与现有的代码一起工作。在全世界，Flutter正在被越来越多的开发者和组织使用，并且Flutter是完全免费、开源的。

## 7、Flutter 特性有哪些？

快速开发（毫秒级热重载）

绚丽UI（内建漂亮的质感设计Material Design和Cupertino Widget和丰富平滑的动画效果和平台感知）

响应式(Reactive，用强大而灵活的API解决2D、动画、手势、效果等难题)

原生访问功能

堪比原生性能

## 8、Flutter 和 Dart的关系是什么？

Flutter是一个使用Dart语言开发的跨平台移动UI框架，通过自建绘制引擎，能高性能、高保真地进行移动开发。Dart囊括了多数编程语言的优点，它更符合Flutter构建界面的方式。

## 9、Widget 和 element 和 RenderObject 之间的关系？

- Widget是用户界面的一部分,并且是不可变的。
- Element是在树中特定位置Widget的实例。
- RenderObject是渲染树中的一个对象，它的层次结构是渲染库的核心。

Widget会被inflate（填充）到Element，并由Element管理底层渲染树。Widget并不会直接管理状态及渲染, 而是通过State这个对象来管理状态。Flutter创Element的可见树，相对于Widget来说，是可变的，通常界面开发中，我们不用直接操作Element,而是由框架层实现内部逻辑。就如一个UI视图树中，可能包含有多个TextWidget(Widget被使用多次)，但是放在内部视图树的视角，这些TextWidget都是填充到一个个独立的Element中。Element会持有renderObject和widget的实例。记住，Widget 只是一个配置，RenderObject 负责管理布局、绘制等操作。

在第一次创建 Widget 的时候，会对应创建一个 Element， 然后将该元素插入树中。如果之后 Widget 发生了变化，则将其与旧的 Widget 进行比较，并且相应地更新 Element。重要的是，Element 不会被重建，只是更新而已。

## 10、mixin extends implement 之间的关系?

继承（关键字 extends）、混入 mixins （关键字 with）、接口实现（关键字 implements）。这三者可以同时存在，前后顺序是extends -> mixins -> implements。

Flutter中的继承是单继承，子类重写超类的方法要用@Override，子类调用超类的方法要用super。

在Flutter中，Mixins是一种在多个类层次结构中复用类代码的方法。mixins的对象是类，mixins绝不是继承，也不是接口，而是一种全新的特性，可以mixins多个类，mixins的使用需要满足一定条件。



## 11、重写运算符，如下所示重载 operator 后对类进行 +/- 操作

## 12、Dart构造方法

## 13、Assert(断言)

## 14、Dart 中数组和 List 是一样的。

## 15、说一下 Stream？

## 16、Future和Isolate有什么区别？

## 17、Stream 与 Future是什么关系？

## 18、Stream 两种订阅模式？

## 19、await for 如何使用?

## 20、Flutter中的Widget、State、Context 的核心概念？是为了解决什么问题？

# 由于篇幅原因，此处仅展示部分内容，查看更多Android面试题点击直接查看

# 全部答案，更新日期：2023年2月7日，直接下载吧！

# 下载链接：[全部答案，整理好了](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

# 新增：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

