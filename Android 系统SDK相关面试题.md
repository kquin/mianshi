# 这套Android面试题汇总大全，希望对大家有帮助哈~

# 全部答案，更新日期：2月7日，直接下载吧

# 全部答案，更新日期：2023年2月7日，直接下载吧！

# 下载链接：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

## 1、请简要谈谈Android系统的架构组成？

android系统分为四部分，从高到低分别是：
1、Android应用层
Android会同一系列核心应用程序包一起发布，该应用程序 包包括email客户端，SMS短消息程序，日历，地图，浏览 器，联系人管理程序等。所有的应用程序都是使用JAVA语言编写的。
2、Android应用框架层
开发人员也可以完全访问核心应用程序所使用的API框架。该应用程序的架构设计简化了组件的重用;任何一个应用程序都可以发布它的功能块并且任何其它的应用程序都可以使用其所发布的功能块(不过得遵循框架的安全性限制)。同样，该应用程序重用机制也使用户可以方便的替换程序组件。
3、Android系统运行层
Android 包含一些C/C++库，这些库能被Android系统中不同的组件使用。它们通过 Android 应用程序框架为开发者提供服务。
4、Linux内核层
Android 的核心系统服务依赖于 Linux 2.6 内核，如安全性，内存管理，进程管理， 网络协议栈和驱动模型。Linux 内核也同时作为硬件和软件栈之间的抽象层。

## 2、SharedPreferences 是线程安全的吗？它的 commit 和 apply 方法有什么区别？
context.getSharedPreferences()开始追踪的话，可以去到ContextImpl的getSharedPreferences()，最终发现SharedPreferencesImpl这个SharedPreferences的实现 类，在代码中可以看到读写操作时都有大量的synchronized，因此它是线程安全的

## 3、Serializable和Parcelable的区别?

Android中序列化有两种方式：Serializable以及Parcelable。其中Serializable是Java自带的，而Parcelable是安卓专有的。
Seralizable相对Parcelable而言，好处就是非常简单，只 需对需要序列化的类class执行就可以，不需要手动去处理 序列化和反序列化的过程，所以常常用于网络请求数据处理，Activity之间传递值的使用。
Parcelable是android特有的序列化API，它的出现是为了解决Serializable在序列化的过程中消耗资源严重的问题， 但是因为本身使用需要手动处理序列化和反序列化过程， 会与具体的代码绑定，使用较为繁琐，一般只获取内存数据的时候使用。

## 4、请简述一下 Android 7.0 的新特性？

1.低电耗功能改进
2.引入画中画功能
3.引入“长按快捷方式”，即App Shortcuts
4.引入混合模式，同时存在解释执行/AOT/JIT，安装应用时默
认不全量编译，使得安装应用时间大大缩短
5.引入了对私有平台库限制，然而用一个叫做Nougat_dlfunctions的库就行
6.不推荐使用file:// URI传递数据，转而推荐使用FileProvider
7.快速回复通知

## 5、谈谈ArrayMap和HashMap的区别？

1.查找效率
HashMap因为其根据hashcode的值直接算出index,所以其 查找效率是随着数组长度增大而增加的。ArrayMap使用的是二分法查找，所以当数组长度每增加一 倍时，就需要多进行一次判断，效率下降
2.扩容数量
HashMap初始值16个长度，每次扩容的时候，直接申请双倍的数组空间。

ArrayMap每次扩容的时候，如果size长度大于8时申请size*1.5个长度，大于4小于8时申请8个，小于4时申请4 个。
这样比较ArrayMap其实是申请了更少的内存空间，但 是扩容的频率会更高。因此，如果数据量比较大的时候，还是使用HashMap更合适，因为其扩容的次数要比ArrayMap少很多。

3.扩 容 效 率
HashMap每次扩容的时候重新计算每个数组成员的位置， 然后 放 到 新 的 位 置 。
ArrayMap则是直接使用System.arraycopy，所以效率上 肯定是ArrayMap更占优势。
4.内存消耗
以ArrayMap采用了一种独特的方式，能够重复的利用因为 数据扩容而遗留下来的数组空间，方便下一个ArrayMap的 使用。而HashMap没有这种设计。 由于ArrayMap之缓存了长度是4和8的时候，所以如果频繁的使用到Map，而且数据量都比较小的时候，ArrayMap无疑是相当的是节省内 存的。
5.总结
综上所述，数据量比较小，并且需要频繁的使用Map存储数据的时候，推荐使用ArrayMap。而数据量比较大的时候，则推荐使用HashMap。

## 6、简要说说 LruCache 的原理？

androidx.collection.LruCache初始化的时候, 会限制缓存占据内存空间的总容量maxSize;
底层维护的是 LinkedHashMap, 使用 LruCache 最好要重写 sizeOf 方法, 用于计算每个被缓存的对象, 在内存中存储时, 占用多少空间;

在 put 操作时, 首先计算新的缓存对象, 占多少空间, 再根据key, 移除老的对象, 占用内存大小 = 之前占用的内存大小 + 新对象的大小 - 老对 象 的 大 小 ;put 操作最后总会根据 maxSize, 在拿到LinkedHashMap.EntrySet中链表的头节点,循环判断,只要当前缓存对象占据内存超出 maxSize,就移除一个头节点, 一直到符合要求;
lruCache 和 LinkedHashMap 的 关 系 :
LinkedHashMap中维护一个双向链表, 并维护 head和tail指针, lruCache 使用了 LinkedHashMap accessOrder 为 true的属性, 只要访问了某个 key, 包括 get 和 put, 就把当前这个 entry 放在链表的位节点, 所以链表的头节点, 是最老访问的节点, 尾节点是最新访问的节点, 所以, lruCache 就很巧妙的利用了这个特点, 完成了 LeastRecently Used 的需求;

## 7、为什么推荐用SparseArray代替HashMap？

并不能替换所有的HashMap。只能替换以int类型为key的HashMap。HashMap中如果以int为key，会强制使用Integer这个包装类型，当我们使用int类型作为key的时候系统会自用装箱成为Integer，这个过程会创建对象一想效率。SparseArray内部是一个int数组和一个object数组。可以直 接使用int减少了自动装箱操作。

## 8、PathClassLoader和DexClassLoader有何区别？

根据art源码来看，两者都继承自BaseDexClassLoader， 最终都会创建一个DexFile，不同点是一个关键的参数：
optimizedDirectory，PathClassLoader 为 null，DexClassLoader则使用传递进来的然后会根据optimizedDirectory判断对应的oat文件是否已经生成（null则使用/data/dalvik-cache/），如果有且该oat对应的dex正确则直接加载，否则触发dex2oat（就是这家伙耗了我们宝贵的时间！！），成功则用生成的oat， 失 败 则 走解 释 执 行
ps：貌似Q版做了优化，不会再卡死在dex2oat里了根据加载外部dex的实验，DexClassLoader会触发dex2oat，而PathClassLoader不会



## 9、说说HttpClient与HttpUrlConnection的区别？并谈谈为何前者会被替代？

## 10、什么是Lifecycle？请分析其内部原理和使用场景？

## 11、谈一谈Android的签名机制？

## 12、谈谈安卓apk构建的流程？

## 13、简述一下Android 8.0、9.0 分别增加了哪些新特性？

## 14、谈谈Android10更新了哪些内容?如何进行适配?

## 15、请简述Apk的安装过程？

## 16、Java与JS代码如何互调？有做过相关优化吗？

## 17、什么是JNI？具体说说如何实现Java与C++的互调？

## 18、请简述从点击图标开始app的启动流程？

# 由于篇幅原因，此处仅展示部分内容，查看更多Android面试题点击直接查看

# 全部答案，更新日期：2023年2月7日，直接下载吧！

# 下载链接：[全部答案，整理好了](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

# 新增：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )



