# 这套Android面试题汇总大全，希望对大家有帮助哈~

# 全部答案，更新日期：2月6日，直接下载吧

# 全部答案，更新日期：2023年2月6日，直接下载吧！

# 下载链接：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )

## 1、Activity 与 Fragment 之间常见的几种通信方式？

viewModel 做数据管理，activity 和 fragment 公用同个viewModel 实现数据传递

## 2、LaunchMode 的应用场景？

LaunchMode 有四种，分别为 Standard， SingleTop，SingleTask 和 SingleInstance，每种模式的实现原理一楼都做了较详细说明，下面说一下具体使用场景：

- Standard：
  Standard模式是系统默认的启动模式，一般我们 app 中大部分页面都是由该模式的页面构成的，比较常见的场景是：社交应用中，点击查看用户A信息->查看用户A 粉丝->在粉丝中挑选查看用户B信息->查看用户A粉丝... 这种情况下一般我们需要保留用户操作 Activity栈的页面所有执行顺序。
- SingleTop:
  SingleTop 模式一般常见于社交应用中的通知栏行为功能，例如：App 用户收到几条好友请求的推送消息，需要用户点击推送通知进入到请求者个人信息页，将信息页设置为 SingleTop 模式就可以增强复用性。
- SingleTask：
  SingleTask 模式一般用作应用的首页，例如浏览器主页，用户可能从多个应用启动浏览器，但主界面仅仅启动一次，其余情况都会走onNewIntent，并且会清空主界面上面的其他页面。
- SingleInstance：
  SingleInstance 模式常应用于独立栈操作的应用，如闹钟的提醒页面，当你在A应用中看视频时，闹钟响了， 你点击闹钟提醒通知后进入提醒详情页面，然后点击返回就再次回到A的视频页面，这样就不会过多干扰到用户先前的操作了。

## 3、BroadcastReceiver 与LocalBroadcastReceiver 有什么区别？

- BroadcastReceiver 是跨应用广播，利用Binder机制实现，支持动态和静态两种方式注册方式。
- LocalBroadcastReceiver 是应用内广播，利用Handler 实现，利用了IntentFilter的match功能，提供消息的发布与接收功能，实现应用内通信，效率和安全性比较 高，仅支持动态注册。

## 4、对于 Context，你了解多少?

Context也叫上下文，是有关应用程序环境的全局信息的接口。这是一个抽象类, 它允许访问特定于应用程序的资源和类，以及对应用程序级操作的调用，比如启动活动，发送广播和接收意图等；

Activity,Service,Application都是 Context的子类。Context 的具体实现类是 ContextImpl, 还有一个包装类ContextWrapper, ContextWrapper 的 子 类 有 Service ，Application，ContextThemeWrapper, Activity 又是ContextThemeWrapper 的子类，ContextThemeWrapper 也可以叫 UI Context，跟UI 操作相关的最好使用此类 Context。ContextWrapper 中有个 mBase，这个 mBase 其实是ContextImpl，它是在Activity, Service, Application 创建时通过attachBaseContext()方法将各自对对应ContextImpl 赋值的。对 context 的操作，最终实现都是在ContextImpl。
对于 startActivity操作
* 当为Activity Context则可直接使用;
* 当为其他Context, 则必须带上FLAG_ACTIVITY_NEW_TASK flags才能使用;因为非 Activitycontext启动 Activity没有 Activity栈，则无法启动，因此需要加开启新的栈;
* 另外UI相关要Activity中使用. getApplication()和getApplicationContext()区别？



1.对于Activity/Service来说,getApplication()和getApplicationContext()的返回值完全相同; 除非厂商修改过接口;

2.BroadcastReceiver在onReceive的过程, 能使用getBaseContext().getApplicationContext获取所在Application, 而无法使用getApplication;3. ContentProvider能使用getContext().getApplicationContext()获取所在

3.ContentProvider能使用getContext().getApplicationContext()获取所在Application. 绝大多数情况下没有问题, 但是有可能会出现空指针的问题, 情况如下:

>当同一个进程有多个apk的情况下, 对于第二个apk是由provider方式拉起的, 前面介绍过provider创建过程并不会初始化所在application, 此时执行getContext().getApplicationContext()返回的结果便是NULL. 所以对于这种情况要做好判空.

## 5、IntentFilter是什么？有哪些使用场景？

IntentService是什么

ntentService是Service的子类，继承与Service类，用于处理需要异步请求。用户通过调用Context.StartService(Intent)发送请求，服务根据需要启 动，使用工作线程依次处理每个Intent，并在处理完所有工 作后自身停止服务。

使用时，扩展IntentService并实现onHandleIntent(android.content.Intent)。IntentService接收Intent，启动工作线程，并在适当时机停止服务。
所有的请求都在同一个工作线程上处理，一次处理一个请求，所以处理完所以的请求可能会花费很长的时间，但由于IntentService是另外了线程来工作，所以保证不会阻止App的主线程。

IntentService与Service的区别
从何时使用，触发方法，运行环境，何时停止四个方面分析。

- 何时使用

Service用于没有UI工作的任务，但不能执行长任务(长时间的任务)，如果需要Service来执行长时间的任务，则必须手动开店一个线程来执行该Service。IntentService可用于执行不与主线程沟通的长任务。

- 触发方法

Service通过调用 startService() 方法来触发。而IntentService通过Intent来触发，开启一个新的工作线 程，并在线程上调用 onHandleIntent() 方法。

- 运行环境

Service 在App主线程上运行，没有与用户交互，即在后台运行，如果执行长时间的请求任务会阻止主线程工作。IntentService在自己单独开启的工作线程上运行，即使执行长时间的请求任务也不会阻止主线程工作。

- 何时停止

如果执行了Service，我们是有责任在其请求任务完成后关 闭服务，通过调用 stopSelf() 或 stopService()来结束服务。IntentService会在执行完所有的请求任务后自行关闭服务，所以我们不必额外调用 stopSelf() 去关闭它。

## 6、谈一谈startService和bindService的区别，生命周期以及使用场景？

## 7、 Service如何进行保活？

## 8、简单介绍下ContentProvider是如何实现数据共享的？

## 9、说下切换横竖屏时Activity的生命周期?

## 10、Activity中onNewIntent方法的调用时机和使用场景？

## 11、Intent传输数据的大小有限制吗？如何解决？

## 12、说说ContentProvider、ContentResolver、ContentObserver 之间
的关系？

## 13、说说Activity加载的流程？

# 由于篇幅原因，此处仅展示部分内容，查看更多Android面试题点击直接查看

# 全部答案，更新日期：2023年2月6日，直接下载吧！

# 下载链接：[全部答案，整理好了](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md)

# 新增：[高清全答案解析，累计935页+大厂面试题 PDF](http://gitee.com/hu-laopi/NewDevBooks/blob/master/docs/index.md )



